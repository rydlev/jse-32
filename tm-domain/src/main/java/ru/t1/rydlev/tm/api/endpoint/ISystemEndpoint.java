package ru.t1.rydlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.ServerAboutRequest;
import ru.t1.rydlev.tm.dto.request.ServerVersionRequest;
import ru.t1.rydlev.tm.dto.response.ServerAboutResponse;
import ru.t1.rydlev.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/SystemEndpoint")
public interface ISystemEndpoint extends IEndpoint {

    @GET
    @NotNull
    @WebMethod
    @Path("/getAbout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerAboutRequest request
    );

    @GET
    @NotNull
    @WebMethod
    @Path("/getVersion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerVersionRequest request
    );

}
