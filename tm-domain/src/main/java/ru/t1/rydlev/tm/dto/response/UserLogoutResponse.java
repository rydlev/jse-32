package ru.t1.rydlev.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResponse {
}
